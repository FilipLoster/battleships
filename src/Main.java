import gamerules.rules.loop.GameLoop;
import utils.di.InjectionScope;
import utils.di.MainScope;
import view.GameView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    static InjectionScope scope;
    static GameView gameView;
    static GameLoop game;

    public static void main(String[] args) {
        selectGameMode();

        initialise();

        runGame();
    }

    private static void selectGameMode() {
        System.out.println("Welcome to BATTLESHIPS");
        System.out.println("Select game mode:");
        System.out.println("(0) Original");
        System.out.println("(1) Salvo");

        String line = readLine();
        try {
            int value = Integer.parseUnsignedInt(line);

            switch(value) {
                case 0:
                    MainScope.pickOriginalEdition();
                    break;

                case 1:
                    MainScope.pickSalvoEdition();
                    break;

                default: selectGameMode();
            }
        } catch (NumberFormatException nfe) {
            selectGameMode();
        }
    }

    private static void initialise() {
        scope = MainScope.getScope();
        game = scope.getGameLoop();
        gameView = scope.getGameView();
    }

    private static void runGame() {
        game.start();

        gameView.bind();
        while(!game.loop()) {}
        gameView.unbind();
    }

    private static String readLine() {
        String result = "";

        try {
            result = bufferedReader.readLine();
        } catch( IOException ioe) {}

        return result;
    }
}
