package gamerules.board;

import gamerules.ships.Ship;
import utils.math.Vector2;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class ShipOnTheGameBoard {
    private Ship ship;
    private ShipBoardPosition position;
    private List<Boolean> shipDamaged;
    private List<BoardPosition> occupiedPositions;

    public boolean isAlive() {
        return shipDamaged.stream().anyMatch((Boolean b) -> !b);
    }

    List<BoardPosition> getOccupiedCells() {
        if(occupiedPositions != null)
            return new ArrayList<>(occupiedPositions);

        occupiedPositions = generateCellsOccupiedByThisShip();

        return getOccupiedCells();
    }

    public ShipBoardPosition getPosition() {
        return (ShipBoardPosition)position.clone();
    }

    ShipOnTheGameBoard(Ship ship, ShipBoardPosition position) {
        super();
        this.ship = ship;
        this.position = position;
        this.shipDamaged = new ArrayList<>();

        for(int i = 0; i < ship.getHeight(); i++)
            this.shipDamaged.add(false);
    }

    void hitShip(BoardPosition position) {
        List<BoardPosition> occupiedCells = getOccupiedCells();

        for (int i = 0; i < occupiedCells.size(); i++) {
            BoardPosition cell = occupiedCells.get(i);

            if(!position.equals(cell)) continue;

            shipDamaged.set(i, true);
            return;
        }

        throw new InvalidParameterException();
    }

    boolean isShipCellHit(BoardPosition position) {
        List<BoardPosition> occupiedCells = getOccupiedCells();

        for (int i = 0; i < occupiedCells.size(); i++) {
            BoardPosition cell = occupiedCells.get(i);

            if (!position.equals(cell)) continue;
            return shipDamaged.get(i);
        }

        throw new InvalidParameterException();
    }

    private ArrayList<BoardPosition> generateCellsOccupiedByThisShip() {
        ArrayList<BoardPosition> result = new ArrayList<>();
        Vector2 direction = position.getDirectionVector();

        for(int i = 0; i < ship.getHeight(); i++)
            result.add(
                    new BoardPosition(
                            position.getColumnPosition() + direction.getX() * i,
                            position.getRowPosition() + direction.getY() * i));

        return result;
    }
}
