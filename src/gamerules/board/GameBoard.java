package gamerules.board;

import gamerules.ships.Ship;

import java.util.List;
import java.util.Optional;

public interface GameBoard {
    int getWidth();
    int getHeight();
    int getDestroyedShipsCount();
    int getAliveShipsCount();
    int getTotalShipsCount();
    List<BoardCellStatus> getBoardShoots();
    boolean isReadyForGame();

    boolean addShip(ShipBoardPosition shipPosition, Ship ship);
    void shootAtPosition(List<BoardPosition> position);
    boolean isShootPositionValid(BoardPosition position);
    Optional<ShipOnTheGameBoard> getShipFromBoardPosition(BoardPosition position);
}
