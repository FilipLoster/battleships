package gamerules.board;

public class BoardPosition {
    private int columnPosition;
    private int rowPosition;

    public int getColumnPosition() {
        return columnPosition;
    }

    public int getRowPosition() {
        return rowPosition;
    }

    public BoardPosition(int columnPosition, int rowPosition) {
        super();
        this.columnPosition = columnPosition;
        this.rowPosition = rowPosition;
    }

    @Override
    protected Object clone() {
        return new BoardPosition(columnPosition, rowPosition);
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof BoardPosition) {
            BoardPosition otherBoardPosition = (BoardPosition)o;
            return compare(otherBoardPosition);
        }

        return super.equals(o);
    }

    public boolean compare(BoardPosition other) {
        return other.getRowPosition() == getRowPosition() &&
               other.getColumnPosition() == getColumnPosition();
    }
}
