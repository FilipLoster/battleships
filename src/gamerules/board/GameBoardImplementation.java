package gamerules.board;

import gamerules.rules.Rules;
import gamerules.ships.Ship;
import utils.eventbus.Event;
import utils.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GameBoardImplementation implements GameBoard {

    private int boardWidth;

    private int boardHeight;

    private ArrayList<ShipOnTheGameBoard> shipsOnTheBoard;

    private ArrayList<BoardCellStatus> boardCellStatuses;

    EventBus bus;

    Rules rules;

    @Override
    public int getWidth() {
        return boardWidth;
    }

    @Override
    public int getHeight() {
        return boardHeight;
    }

    @Override
    public int getAliveShipsCount() {
        return (int)shipsOnTheBoard.stream().filter(ShipOnTheGameBoard::isAlive).count();
    }

    @Override
    public int getTotalShipsCount() {
        return shipsOnTheBoard.size();
    }

    @Override
    public int getDestroyedShipsCount() {
        return this.getTotalShipsCount() - getAliveShipsCount();
    }

    @Override
    public boolean isReadyForGame() {
        return getTotalShipsCount() == rules.getPlayerShipSet().length;
    }

    @Override
    public List<BoardCellStatus> getBoardShoots() {
        return new ArrayList<>(boardCellStatuses);
    }

    GameBoardImplementation(int boardWidth, int boardHeight) {
        super();
        this.shipsOnTheBoard = new ArrayList<>();
        this.boardCellStatuses = new ArrayList<>();
        this.boardWidth = boardWidth;
        this.boardHeight = boardHeight;
    }

    @Override
    public boolean addShip(ShipBoardPosition shipPosition, Ship ship) {
        ShipOnTheGameBoard wrapper = new ShipOnTheGameBoard(ship, shipPosition);
        boolean result = canAddShip(wrapper) && shipsOnTheBoard.add(wrapper);

        if(result) {
            Event.BoardChanged.setParam(this);
            bus.publishEvent(Event.BoardChanged);
        } else {
            Event.Error.setParam("Can not add ship - invalid position");
            bus.publishEvent(Event.Error);
        }

        return result;
    }

    @Override
    public void shootAtPosition(List<BoardPosition> positions) {
        for(BoardPosition position : positions) {
            if(!isShootPositionValid(position)) continue;

            Optional<ShipOnTheGameBoard> potentialShip = getShipFromBoardPosition(position);

            if(potentialShip.isPresent()) {
                potentialShip.get().hitShip(position);
                recordShoot(new BoardCellStatus(position, ShootResult.HIT));
            } else {
                recordShoot(new BoardCellStatus(position, ShootResult.MISS));
            }
        }

        Event.BoardChanged.setParam(this);
        bus.publishEvent(Event.BoardChanged);
    }

    @Override
    public boolean isShootPositionValid(BoardPosition position) {
        if(!isBoardPositionValid(position)) return false;

        return getShipFromBoardPosition(position)
            .map(shipOnTheGameBoard -> !shipOnTheGameBoard.isShipCellHit(position))
            .orElse(true);
    }

    @Override
    public Optional<ShipOnTheGameBoard> getShipFromBoardPosition(BoardPosition position) {
        return isBoardPositionValid(position) ?
            shipsOnTheBoard
                .stream()
                .filter(shipOnTheGameBoard ->
                        shipOnTheGameBoard
                                .getOccupiedCells()
                                .stream()
                                .anyMatch(boardPosition -> boardPosition.equals(position)))
                .findFirst() :
            Optional.empty();
    }

    private boolean canAddShip(ShipOnTheGameBoard ship) {
        return ship.getOccupiedCells().stream()
            .allMatch(boardPosition -> isBoardPositionValid(boardPosition) && !isBoardPositionOccupied(boardPosition));
    }

    private void recordShoot(BoardCellStatus status) {
        if(boardCellStatuses.contains(status)) return;
        boardCellStatuses.add(status);
    }

    private boolean isBoardPositionOccupied(BoardPosition position) {
        return getShipFromBoardPosition(position).isPresent();
    }

    private boolean isBoardPositionValid(BoardPosition position) {
        return
            (position.getColumnPosition() >= 0) &&
            (position.getRowPosition() >= 0) &&
            (position.getColumnPosition() < getWidth()) &&
            (position.getRowPosition() < getHeight());
    }
}
