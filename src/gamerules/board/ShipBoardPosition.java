package gamerules.board;

import utils.math.Vector2;

public class ShipBoardPosition extends BoardPosition {
    private PlacementDirection direction;

    public PlacementDirection getDirection() {
        return direction;
    }

    public Vector2 getDirectionVector() {
        switch(getDirection()) {
            case Vertical:   return new Vector2(0, 1);
            case Horizontal: return new Vector2(1, 0);
            default:         return new Vector2(0, 0);
        }
    }

    public ShipBoardPosition(int columnPosition, int rowPosition, PlacementDirection direction) {
        super(columnPosition, rowPosition);
        this.direction = direction;
    }

    public ShipBoardPosition(BoardPosition position, PlacementDirection direction) {
        this(position.getColumnPosition(), position.getRowPosition(), direction);
    }

    @Override
    protected Object clone() {
        return new ShipBoardPosition(this.getColumnPosition(), this.getRowPosition(), this.getDirection());
    }
}
