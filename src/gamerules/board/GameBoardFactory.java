package gamerules.board;

import utils.di.MainScope;

public class GameBoardFactory {


    public static GameBoard getGameBoard() {
        GameBoardImplementation gameBoard
            = new GameBoardImplementation(
                MainScope.getScope().getRules().boardWidth(),
                MainScope.getScope().getRules().boardHeight());

        gameBoard.bus = MainScope.getScope().getEventBus();
        gameBoard.rules = MainScope.getScope().getRules();

        return gameBoard;
    }
}
