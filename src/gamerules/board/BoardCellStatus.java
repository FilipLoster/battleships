package gamerules.board;

public class BoardCellStatus extends BoardPosition {
    private ShootResult shootResult;

    public ShootResult getShootResult() {
        return shootResult;
    }

    public BoardCellStatus(int columnPosition, int rowPosition, ShootResult shootResult) {
        super(columnPosition, rowPosition);
        this.shootResult = shootResult;
    }

    public BoardCellStatus(BoardPosition position, ShootResult shootResult) {
        this(position.getColumnPosition(), position.getRowPosition(), shootResult);
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof BoardCellStatus) {
            BoardPosition otherBoardPosition = (BoardPosition)o;
            return super.compare(otherBoardPosition);
        }

        return super.equals(o);
    }
}
