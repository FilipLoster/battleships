package gamerules.board;

public enum PlacementDirection {
    Horizontal,
    Vertical
}
