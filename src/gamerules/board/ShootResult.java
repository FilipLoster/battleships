package gamerules.board;

public enum ShootResult {
    MISS,
    HIT
}
