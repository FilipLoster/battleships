package gamerules.player;

public class HumanPlayerFactory extends AbstractPlayerFactory {
    @Override
    public Player getPlayer() {
        return new HumanPlayerImplementation();
    }
}
