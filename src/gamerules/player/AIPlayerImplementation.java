package gamerules.player;

import gamerules.board.BoardPosition;
import gamerules.board.PlacementDirection;
import gamerules.board.ShipBoardPosition;
import gamerules.ships.Ship;
import gamerules.ships.ShipFactoryProducer;
import utils.Callback;

import java.util.Random;

public class AIPlayerImplementation implements Player {
    Random random;

    public AIPlayerImplementation() {
        super();
        random = new Random();
    }

    @Override
    public boolean requireInterface() {
        return false;
    }

    @Override
    public void askForShipPosition(Ship ship, Callback<ShipBoardPosition> onShipPositionDecided) {
        ShipBoardPosition newPosition = new ShipBoardPosition(
                random.nextInt(10 - ship.getHeight()),
                random.nextInt(10 - ship.getHeight()),
                random.nextBoolean() ?
                    PlacementDirection.Horizontal :
                    PlacementDirection.Vertical);
        onShipPositionDecided.call(newPosition);
    }

    @Override
    public void askForNextShoot(Callback<BoardPosition> onNextShootDecided) {
        onNextShootDecided.call(new BoardPosition(
            random.nextInt(10),
            random.nextInt(10)));
    }
}
