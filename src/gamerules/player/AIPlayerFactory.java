package gamerules.player;

public class AIPlayerFactory extends AbstractPlayerFactory {
    @Override
    public Player getPlayer() {
        return new AIPlayerImplementation();
    }
}
