package gamerules.player;

public abstract class AbstractPlayerFactory {
    public abstract Player getPlayer();
}
