package gamerules.player;

import gamerules.board.BoardPosition;
import gamerules.board.ShipBoardPosition;
import gamerules.ships.Ship;
import gamerules.ships.ShipFactoryProducer;
import utils.Callback;

public interface Player {
    boolean requireInterface();
    void askForShipPosition(Ship ship, Callback<ShipBoardPosition> onShipPositionDecided);
    void askForNextShoot(Callback<BoardPosition> onNextShootDecided);
}
