package gamerules.player;

import gamerules.board.BoardPosition;
import gamerules.board.PlacementDirection;
import gamerules.board.ShipBoardPosition;
import gamerules.ships.Ship;
import gamerules.ships.ShipFactoryProducer;
import utils.Callback;
import utils.eventbus.Event;
import utils.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HumanPlayerImplementation implements Player {
    EventBus bus;

    BufferedReader bufferedReader;

    public HumanPlayerImplementation() {
        super();
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public boolean requireInterface() {
        return true;
    }

    @Override
    public void askForShipPosition(Ship ship, Callback<ShipBoardPosition> onShipPositionDecided) {
        System.out.println("Placing ship("+ship.getHeight()+")");

        onShipPositionDecided.call(
            new ShipBoardPosition(
                getPositionFromUser(),
                getPlacementDirectionFromUser()
            ));
    }

    @Override
    public void askForNextShoot(Callback<BoardPosition> onNextShootDecided) {
        System.out.println("Shooting at enemy");
        onNextShootDecided.call(getPositionFromUser());
    }

    private BoardPosition getPositionFromUser() {
        int x, y;

        System.out.println("Enter X and Y:");
        String line = readLine();
        String numbers[] = line.split(" ");

        if(numbers.length != 2) {
            return getPositionFromUser();
        }

        try {
            x = Integer.parseUnsignedInt(numbers[0]);
            y = Integer.parseUnsignedInt(numbers[1]);
        } catch (NumberFormatException nbe) {
            return getPositionFromUser();
        }

        return new BoardPosition(x, y);
    }

    private PlacementDirection getPlacementDirectionFromUser() {
        int x;

        try {
            System.out.println("Horizontal (0) or vertical (1):");
            String line = readLine();
            x = Integer.parseInt(line);

        } catch (NullPointerException | NumberFormatException exception) {
            Event.Error.setParam("Please enter valid number");
            bus.publishEvent(Event.Error);
            return getPlacementDirectionFromUser();
        }

        return x == 0 ? PlacementDirection.Horizontal : PlacementDirection.Vertical;
    }

    private String readLine() {
        String result = "";

        try {
            result = bufferedReader.readLine();
        } catch( IOException ioe) {}

        return result;
    }
}
