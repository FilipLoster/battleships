package gamerules.player;

public class PlayerFactoryProducer {
    public enum AIDifficulty {
        Easy,
        Medium,
        Hard
    }

    public static AbstractPlayerFactory getHumanPlayerFactory() {
        return new HumanPlayerFactory();
    }

    public static AbstractPlayerFactory getAIPlayerFactory(AIDifficulty difficulty) {
        return new AIPlayerFactory();
    }
}
