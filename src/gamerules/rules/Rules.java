package gamerules.rules;

import gamerules.rules.participants.GameParticipant;
import gamerules.ships.ShipFactoryProducer;

import java.util.ArrayList;

public interface Rules {
    ShipFactoryProducer.ShipTypes[] getPlayerShipSet();
    int guessesPerTurn(GameParticipant participant);
    int boardWidth();
    int boardHeight();
}
