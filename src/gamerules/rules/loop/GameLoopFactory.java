package gamerules.rules.loop;

import utils.di.InjectionScope;
import utils.di.MainScope;

public class GameLoopFactory {

    private static InjectionScope scope = MainScope.getScope();

    public static GameLoop getGameLoop() {
        GameLoopImplementation implementation = new GameLoopImplementation();

        implementation.participants.add(scope.getHumanParticipant());
        implementation.participants.add(scope.getAIParticipant());

        implementation.gameRules = scope.getRules();
        implementation.bus = scope.getEventBus();

        return implementation;
    }
}
