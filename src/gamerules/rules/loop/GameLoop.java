package gamerules.rules.loop;

public interface GameLoop {
    void start();
    boolean loop();
}
