package gamerules.rules.loop;

import gamerules.board.BoardPosition;
import gamerules.board.ShipBoardPosition;
import gamerules.rules.Rules;
import gamerules.rules.participants.GameParticipant;
import gamerules.ships.Ship;
import gamerules.ships.ShipFactoryProducer;
import utils.Action;
import utils.Callback;
import utils.eventbus.Event;
import utils.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Stack;

public class GameLoopImplementation implements GameLoop {
    private int currentPlayerTurn = 0;
    private Stack<Action> pendingActions;

    ArrayList<GameParticipant> participants;
    Rules gameRules;
    EventBus bus;

    class ShipPositionPickerFactory {
        GameParticipant participant;
        Ship ship;

        ShipPositionPickerFactory(GameParticipant participant, ShipFactoryProducer.ShipTypes shipType) {
            super();
            this.participant = participant;
            this.ship = ShipFactoryProducer.getFactory(shipType).getShip();
        }

        void run() {
            Event.EnableInterface.setParam(participant.getPlayer().requireInterface());
            bus.publishEvent(Event.EnableInterface);

            participant
                .getPlayer()
                .askForShipPosition(ship, create());
        }

        private Callback<ShipBoardPosition> create() {
            return shipBoardPosition -> {
                participant.getBoard().addShip(shipBoardPosition, ship);

                pendingActions.push(GameLoopImplementation.this::setUpNextParticipantShip);
            };
        }
    }

    class ShootTargetPickerFactory {
        GameParticipant participant;
        GameParticipant opponent;
        int remainingShoots;
        ArrayList<BoardPosition> shootPositions;

        ShootTargetPickerFactory(GameParticipant participant, GameParticipant opponent) {
            super();
            this.participant = participant;
            this.opponent = opponent;
            this.shootPositions = new ArrayList<>();
            remainingShoots = gameRules.guessesPerTurn(participant);
        }

        void run() {
            Event.EnableInterface.setParam(participant.getPlayer().requireInterface());
            bus.publishEvent(Event.EnableInterface);

            if(remainingShoots > 0) {
                pendingActions.add(() -> participant.getPlayer().askForNextShoot(create()));
            } else {
                Event.EnableInterface.setParam(true);
                bus.publishEvent(Event.EnableInterface);

                opponent.getBoard().shootAtPosition(shootPositions);

                pendingActions.push(GameLoopImplementation.this::nextTurn);
            }
        }

        private Callback<BoardPosition> create() {
            return boardPosition -> {
                if(opponent.getBoard().isShootPositionValid(boardPosition) &&
                        !shootPositions.contains(boardPosition)) {
                    shootPositions.add(boardPosition);
                    remainingShoots--;
                }

                run();
            };
        }
    }

    GameLoopImplementation() {
        super();
        participants = new ArrayList<>();
        pendingActions = new Stack<>();
    }

    private boolean hasGameEnded() {
        return participants.stream().filter(GameParticipant::isAlive).count() <= 1;
    }

    @Override
    public void start() {
        pendingActions.push(this::setUpNextParticipantShip);
    }

    @Override
    public boolean loop() {
        if (hasGameEnded()) {
            Event.EnableInterface.setParam(true);
            bus.publishEvent(Event.EnableInterface);

            Event.GameOver.setParam(participants.stream().filter(GameParticipant::isAlive).findFirst());
            bus.publishEvent(Event.GameOver);
            return true;
        }

        if(pendingActions.isEmpty()) return false;
        pendingActions.pop().call();

        return false;
    }

    private void setUpNextParticipantShip() {
        for(GameParticipant p : participants)
            if(!p.getBoard().isReadyForGame()) {
                ShipFactoryProducer.ShipTypes missingShipType
                    = gameRules.getPlayerShipSet()[p.getBoard().getTotalShipsCount()];

                pendingActions.push(new ShipPositionPickerFactory(p, missingShipType)::run);
                return;
            }

        pendingActions.push(this::nextTurn);
    }

    private void nextTurn() {
        GameParticipant currentParticipant = participants.get(currentPlayerTurn);

        currentPlayerTurn = (currentPlayerTurn + 1) % participants.size();
        GameParticipant opponent = participants.get(currentPlayerTurn);

        pendingActions.push(new ShootTargetPickerFactory(currentParticipant, opponent)::run);
    }
}
