package gamerules.rules.participants;

import gamerules.board.GameBoard;
import gamerules.player.Player;

public class GameParticipantImplementation implements GameParticipant {
    protected GameBoard board;
    protected Player player;

    @Override
    public boolean isAlive() {
        return !board.isReadyForGame() ||
               board.getAliveShipsCount() != 0;
    }

    @Override
    public GameBoard getBoard() {
        return board;
    }

    @Override
    public Player getPlayer() {
        return player;
    }
}
