package gamerules.rules.participants;

import gamerules.board.GameBoard;
import gamerules.player.Player;

public interface GameParticipant {
    boolean isAlive();
    GameBoard getBoard();
    Player getPlayer();
}
