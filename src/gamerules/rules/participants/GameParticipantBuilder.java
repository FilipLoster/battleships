package gamerules.rules.participants;

import utils.di.MainScope;

public class GameParticipantBuilder {
    protected GameParticipantImplementation participant;

    public GameParticipantBuilder() {
        super();
        participant = new GameParticipantImplementation();
    }

    public GameParticipant getParticipant() {
        GameParticipant result = participant;
        participant = null;

        try {
            validateParticipant(result);
        } catch (Exception e) {
            System.out.println("Error during construction of game participant");
            return null;
        }

        return result;
    }

    public void setDefaultBoard() {
        participant.board = MainScope.getScope().getDefaultBoard();
    }

    public void setAsHumanPlayer() {
        participant.player = MainScope.getScope().getHumanPlayerFactory().getPlayer();
    }

    public void setAsAIPlayer() {
        participant.player = MainScope.getScope().getAIPlayerFactory().getPlayer();
    }

    private void validateParticipant(GameParticipant participant) throws Exception {
        if(participant == null)
            throw new NullPointerException();
        if(participant.getBoard() == null)
            throw new IllegalStateException();
        if(participant.getPlayer() == null)
            throw new IllegalStateException();
    }
}
