package gamerules.rules;

import gamerules.rules.participants.GameParticipant;
import gamerules.ships.ShipFactoryProducer;

public class OriginalRules implements Rules {
    ShipFactoryProducer.ShipTypes[] shipList = {
            ShipFactoryProducer.ShipTypes.FiveDecker,
            ShipFactoryProducer.ShipTypes.FourDecker,
            ShipFactoryProducer.ShipTypes.ThreeDecker,
            ShipFactoryProducer.ShipTypes.ThreeDecker,
            ShipFactoryProducer.ShipTypes.TwoDecker,
    };

    @Override
    public ShipFactoryProducer.ShipTypes[] getPlayerShipSet() {
        return shipList.clone();
    }

    @Override
    public int guessesPerTurn(GameParticipant participant) {
        return 1;
    }

    @Override
    public int boardWidth() {
        return 10;
    }

    @Override
    public int boardHeight() {
        return 10;
    }
}
