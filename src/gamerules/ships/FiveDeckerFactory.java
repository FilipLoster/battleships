package gamerules.ships;

public class FiveDeckerFactory extends AbstractShipFactory {
    public class FiveDecker implements Ship {
        @Override
        public int getHeight() {
            return 5;
        }
    }

    @Override
    public Ship getShip() {
        return new FiveDecker();
    }
}