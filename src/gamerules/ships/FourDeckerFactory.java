package gamerules.ships;

public class FourDeckerFactory extends AbstractShipFactory {
    public class FourDecker implements Ship {
        @Override
        public int getHeight() {
            return 4;
        }
    }

    @Override
    public Ship getShip() {
        return new FourDecker();
    }
}
