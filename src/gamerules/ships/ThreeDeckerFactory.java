package gamerules.ships;

public class ThreeDeckerFactory extends AbstractShipFactory {
    public class ThreeDecker implements Ship {
        @Override
        public int getHeight() {
            return 3;
        }
    }

    @Override
    public Ship getShip() {
        return new ThreeDecker();
    }
}
