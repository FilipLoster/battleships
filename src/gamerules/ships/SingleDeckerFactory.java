package gamerules.ships;

public class SingleDeckerFactory extends AbstractShipFactory {
    public class SingleDecker implements Ship {
        @Override
        public int getHeight() {
            return 1;
        }
    }

    @Override
    public Ship getShip() {
        return new SingleDecker();
    }
}
