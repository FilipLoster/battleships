package gamerules.ships;

public class ShipFactoryProducer {
    public enum ShipTypes {
        SingleDecker,
        TwoDecker,
        ThreeDecker,
        FourDecker,
        FiveDecker
    }

    public static AbstractShipFactory getFactory(ShipTypes shipType) {
        switch(shipType) {
            case SingleDecker:
                return new SingleDeckerFactory();
            case TwoDecker:
                return new TwoDeckerFactory();
            case ThreeDecker:
                return new ThreeDeckerFactory();
            case FourDecker:
                return new FourDeckerFactory();
            case FiveDecker:
                return new FiveDeckerFactory();
            default:
                return null;
        }
    }
}
