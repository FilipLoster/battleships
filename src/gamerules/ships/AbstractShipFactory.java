package gamerules.ships;

public abstract class AbstractShipFactory {
    public abstract Ship getShip();
}
