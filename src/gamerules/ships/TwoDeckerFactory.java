package gamerules.ships;

public class TwoDeckerFactory extends AbstractShipFactory {
    public class TwoDecker implements Ship {
        @Override
        public int getHeight() {
            return 2;
        }
    }

    @Override
    public Ship getShip() {
        return new TwoDecker();
    }
}
