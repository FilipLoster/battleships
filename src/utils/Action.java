package utils;

public interface Action {
    public void call();
}
