package utils.math;

public class Vector2 {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Vector2(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }
}
