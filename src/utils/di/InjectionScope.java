package utils.di;

import gamerules.board.GameBoard;
import gamerules.player.AbstractPlayerFactory;
import gamerules.player.PlayerFactoryProducer;
import gamerules.rules.Rules;
import gamerules.rules.loop.GameLoop;
import gamerules.rules.participants.GameParticipant;
import gamerules.ships.ShipFactoryProducer;
import utils.eventbus.EventBus;
import view.GameView;

public interface InjectionScope {
    EventBus getEventBus();

    AbstractPlayerFactory getHumanPlayerFactory();
    AbstractPlayerFactory getAIPlayerFactory();

    GameBoard getDefaultBoard();
    Rules getRules();

    GameParticipant getHumanParticipant();
    GameParticipant getAIParticipant();

    GameLoop getGameLoop();

    GameView getGameView();
}
