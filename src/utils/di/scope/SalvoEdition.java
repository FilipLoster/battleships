package utils.di.scope;

import gamerules.player.AbstractPlayerFactory;
import gamerules.player.PlayerFactoryProducer;
import gamerules.rules.Rules;
import gamerules.rules.SalvoRules;

public class SalvoEdition extends BaseScope {
    Rules rules = new SalvoRules();

    @Override
    public AbstractPlayerFactory getAIPlayerFactory() {
        return PlayerFactoryProducer.getAIPlayerFactory(PlayerFactoryProducer.AIDifficulty.Medium);
    }

    @Override
    public Rules getRules() {
        return rules;
    }
}
