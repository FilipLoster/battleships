package utils.di.scope;

import gamerules.board.GameBoard;
import gamerules.board.GameBoardFactory;
import gamerules.player.AbstractPlayerFactory;
import gamerules.player.PlayerFactoryProducer;
import gamerules.rules.loop.GameLoop;
import gamerules.rules.loop.GameLoopFactory;
import gamerules.rules.participants.GameParticipant;
import gamerules.rules.participants.GameParticipantBuilder;
import gamerules.ships.ShipFactoryProducer;
import utils.di.InjectionScope;
import utils.eventbus.EventBus;
import utils.eventbus.EventBusImplementation;
import view.GameView;
import view.GameViewFactory;

abstract public class BaseScope implements InjectionScope {
    EventBus eventBus = new EventBusImplementation();

    @Override
    public AbstractPlayerFactory getHumanPlayerFactory() {
        return PlayerFactoryProducer.getHumanPlayerFactory();
    }

    @Override
    public EventBus getEventBus() {
        return eventBus;
    }

    @Override
    public GameBoard getDefaultBoard() {
        return GameBoardFactory.getGameBoard();
    }

    @Override
    public GameParticipant getHumanParticipant() {
        GameParticipantBuilder builder = new GameParticipantBuilder();
        builder.setDefaultBoard();
        builder.setAsHumanPlayer();
        return builder.getParticipant();
    }

    @Override
    public GameParticipant getAIParticipant() {
        GameParticipantBuilder builder = new GameParticipantBuilder();
        builder.setAsAIPlayer();
        builder.setDefaultBoard();
        return builder.getParticipant();
    }

    @Override
    public GameLoop getGameLoop() {
        return GameLoopFactory.getGameLoop();
    }

    @Override
    public GameView getGameView() {
        return GameViewFactory.getGameView();
    }
}
