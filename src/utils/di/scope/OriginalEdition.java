package utils.di.scope;

import gamerules.player.AbstractPlayerFactory;
import gamerules.player.PlayerFactoryProducer;
import gamerules.rules.OriginalRules;
import gamerules.rules.Rules;

public class OriginalEdition extends BaseScope {
    Rules rules = new OriginalRules();

    @Override
    public Rules getRules() {
        return rules;
    }

    @Override
    public AbstractPlayerFactory getAIPlayerFactory() {
        return PlayerFactoryProducer.getAIPlayerFactory(PlayerFactoryProducer.AIDifficulty.Medium);
    }
}
