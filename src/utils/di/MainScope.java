package utils.di;

import utils.di.scope.OriginalEdition;
import utils.di.scope.SalvoEdition;

public class MainScope {

    private static InjectionScope salvoEdition = new SalvoEdition();
    private static InjectionScope originalEdition = new OriginalEdition();

    private static InjectionScope currentScope;

    public static InjectionScope getScope() {
        return currentScope;
    }

    public static void pickOriginalEdition() {
        currentScope = originalEdition;
    }

    public static void pickSalvoEdition() {
        currentScope = salvoEdition;
    }
}
