package utils;

public interface Callback <P> {
    void call(P p);
}
