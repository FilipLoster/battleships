package utils.eventbus;

public enum Event {
    BoardChanged,
    Error,
    GameOver,
    EnableInterface;

    Object param;

    Event() {}

    public Object getParam() {
        return param;
    }

    public void setParam(Object value) {
        param = value;
    }
}
