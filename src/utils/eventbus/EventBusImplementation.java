package utils.eventbus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.Function;

public class EventBusImplementation implements EventBus {
    HashMap<Event, ArrayList<EventSubscriber>> allSubscribers = new HashMap<>();

    @Override
    public void publishEvent(Event event) {
        Optional
            .ofNullable(allSubscribers.get(event))
            .map(subscribers -> {
                for(EventSubscriber subscriber : subscribers)
                    subscriber.onEvent(event);

                return null;
            });
    }

    @Override
    public void subscribeToEvent(Event event, EventSubscriber subscriber) {
        if(!allSubscribers.containsKey(event))
            allSubscribers.put(event, new ArrayList<EventSubscriber>());

        Optional
            .ofNullable(allSubscribers.get(event))
            .map(subscribers -> {
                if(subscribers.contains(subscriber)) return null;

                subscribers.add(subscriber);

                return null;
            });
    }

    @Override
    public void unsubscribeFromEvent(Event event, EventSubscriber subscriber) {
        Optional
            .ofNullable(allSubscribers.get(event))
            .map(subscribers -> {
                subscribers.remove(subscriber);

                return null;
            });
    }

    @Override
    public void unsubscribeFromAllEvents(EventSubscriber subscriber) {
        for(Event key : allSubscribers.keySet())
            allSubscribers
                .get(key)
                .remove(subscriber);
    }
}
