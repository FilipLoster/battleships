package utils.eventbus;

public interface EventBus {
    void publishEvent(Event event);
    void subscribeToEvent(Event event, EventSubscriber subscriber);
    void unsubscribeFromEvent(Event event, EventSubscriber subscriber);
    void unsubscribeFromAllEvents(EventSubscriber subscriber);
}
