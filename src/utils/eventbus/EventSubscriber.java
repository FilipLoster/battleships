package utils.eventbus;

public interface EventSubscriber {
    void onEvent(Event event);
}
