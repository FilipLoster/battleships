package view;

import gamerules.board.BoardCellStatus;
import gamerules.board.BoardPosition;
import gamerules.board.GameBoard;
import gamerules.board.ShipOnTheGameBoard;
import utils.eventbus.Event;
import utils.eventbus.EventBus;
import utils.eventbus.EventSubscriber;

import java.util.List;
import java.util.Optional;

public class GameViewImplementation implements GameView, EventSubscriber {

    protected EventBus bus;

    private boolean interfaceEnabled = false;

    @Override
    public void bind() {
        bus.subscribeToEvent(Event.BoardChanged, this);
        bus.subscribeToEvent(Event.GameOver, this);
        bus.subscribeToEvent(Event.Error, this);
        bus.subscribeToEvent(Event.EnableInterface, this);
    }

    @Override
    public void unbind() {
        bus.unsubscribeFromAllEvents(this);
    }

    @Override
    public void onEvent(Event event) {
        switch (event) {
            case Error:
                if(!interfaceEnabled) break;

                String errorParam = (String)event.getParam();
                drawError(errorParam);
                break;

            case GameOver:
                drawGameOver();
                break;

            case BoardChanged:
                if(!interfaceEnabled) break;

                GameBoard board = (GameBoard)event.getParam();
                drawGameBoard(board);
                break;

            case EnableInterface:
                interfaceEnabled = (boolean)event.getParam();
                break;
        }
    }

    private void drawError(String errorValue) {
        System.out.println("Error: " + errorValue);
    }

    private void drawGameOver() {
        System.out.println("Game Over!");
    }

    private void drawGameBoard(GameBoard board) {
        clearConsole();

        List<BoardCellStatus> shoots = board.getBoardShoots();
        for (int y = -1; y <= board.getHeight() + 1; y++) {
            for(int x = -1; x <= board.getWidth(); x++) {
                boolean borderDrawn = drawBorder(x, y, board.getWidth(), board.getHeight());
                if(borderDrawn) continue;

                BoardPosition position = new BoardPosition(x, y);
                int shootPosition = shoots.indexOf(position);
                Optional<ShipOnTheGameBoard> shipOnBoard = board.getShipFromBoardPosition(position);

                if(shootPosition == -1) {
                    if(shipOnBoard.isPresent() && !board.isReadyForGame()) {
                        System.out.print("◌");
                    } else {
                        System.out.print(" ");
                    }
                } else {
                    BoardCellStatus cellStatus = shoots.get(shootPosition);
                    switch(cellStatus.getShootResult()) {
                        case HIT:
                            if(!shipOnBoard.isPresent()) continue;

                            if(shipOnBoard.get().isAlive())
                                System.out.print("□");
                            else
                                System.out.print("■");

                            break;

                        case MISS:
                            System.out.print("○");
                            break;
                    }
                }
            }
        }
    }

    private boolean drawBorder(int x, int y, int width, int height) {
        if (y == height + 1) {
            if(x == -1)
                System.out.print("  ");
            else if(x == width)
                System.out.print("\n");
            else
                System.out.print(x);
            return true;
        }

        if(x == -1 && y == -1) {
            System.out.print(" ┌");
            return true;
        }

        if(x == width && y == -1) {
            System.out.print("┐");
            System.out.println();
            return true;
        }

        if(x == width && y == height) {
            System.out.print("┘");
            System.out.println();
            return true;
        }

        if(x == -1 && y == height) {
            System.out.print(" └");
            return true;
        }

        if (x == -1 || x == width) {
            if(x == -1) System.out.print(y);

            System.out.print("│");
            if(x == width) {
                System.out.println();
            }
            return true;
        }

        if (y == -1 || y == height) {
            System.out.print("─");
            return true;
        }

        return false;
    }

    private void clearConsole() {
        System.out.print("\033[H\033[2J");
    }
}
