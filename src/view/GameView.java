package view;

public interface GameView {
    void bind();
    void unbind();
}
