package view;

import utils.di.MainScope;

public class GameViewFactory {
    public static GameView getGameView() {
        GameViewImplementation view = new GameViewImplementation();
        view.bus = MainScope.getScope().getEventBus();
        return view;
    }
}
