# Gra w Statki

Projekt w języku java realizujacy grę w statki w oknie konsoli. Gracz moze wybrać między dwoma trybami rozgrywki oryginalna i salvo

## Uruchomienie Gry
Gra została napisana w języku java w wersji 8, i skompilowana przy użyciu SDK java 1.8.
Nie używa żadnych dodatkowych bibliotek poza domyślnie dostarczonymi wraz z kompilatorem.

## Uruchomienie pliku .jar
Zbudowana wersja gry do pliku .jar znajduje się w folderze builds. Aby ją uruchomić w systemie windows trzeba otworzyć terminal w folderze zawierającym plik battleships.jar i uruchomic polecenie:
java -jar battleships.jar

## Tryby Rozgrywki
- Original. Klasyczny tryb rozgrywki w którym flota gracza składa się z pięciu statków a rozgywka polega na naprzemiennym typowaniu pól na planszy które zostają ostrzelane. Gra kończy się w momencie gdy jedcen z graczy zatopi wszystkie statki przeciwnika
- Salvo. Tryb rozgrywki w którym flota gracza składa się z pięciu statków a rozgrywka polega na naprzemiennym typowaniu pól na planszy które zostają ostrzelane. Ilośc typowanych pól zależy od ilosci statkow gracza które nie zostały jeszcze zatopione. Po zakończeniu typowania przeciwnik oglasza jednocześnie liczbe trafionych i zatopionych statków. Gra kończy się w momencie gdy jeden z graczy zatopi wszystkie statki przeciwinika.

##Użyte Rozwiązania
- Factory: Gra używa wzorca fabryki, oraz fabryki abstrakcyjnej do konstruowania obiektów statkow, planszy do gry oraz graczy.

- Dependency Injection: Gra używa prostego mechanizmu wstrzykiwania zależności aby rozwiązac zależnosci między modułami gry

- Event Bus: Gra używa prostej implementacji szyny wydarzeń do komunikacji między logiką gry a widokiem. Dzięki takiemu rozdzieleniu widoku i logiki grę można teoretycznie uruchomić w trybie "headless", bądź całkowicie zmienić sposob wyświetlania rozgrywki bez zmian kodu działania gry

- Budowniczy: Gra używa wzorca budowniczy do konstrukcji obiektu zawierającego informacje o uczestniku gry. Obiekt ten agreguje w sobie informacje o graczu oraz obecnym stanie jego planszy

- Monady: Gra używa rozwiązań z dziedziny programowania funkcyjnego, w szczególności monady Optional do odpytywania planszy o statki pod wskazaną pozycją oraz wykonywania operacji związanych ze zmianem stanu planszy